# `css-converter`

> Simple webapp to quickly transform CSS declarations into JS objects,
and vice-versa

Available in https://css-transformer.surge.sh
